set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

Plugin 'tpope/vim-fugitive'
"if v:version > 705
"  Plugin 'Valloric/YouCompleteMe'
"endif
Plugin 'parnmatt/vim-root'
Bundle 'powerline/powerline', {'rtp': 'powerline/bindings/vim/'}
"Plugin 'ascenator/L9', {'name': 'newL9'}
Plugin 'godlygeek/tabular'
Plugin 'altercation/vim-colors-solarized'
Plugin 'octol/vim-cpp-enhanced-highlight'
Plugin 'joshdick/onedark.vim'

call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on

let g:ycm_global_ycm_extra_conf = '/vol0/mathieu/.vim/bundle/YouCompleteMe/.ycm_extra_conf.py'

filetype on
syntax enable

set background=dark
colorscheme solarized

set mouse=a

set scrolloff=2

"autocmd BufReadPost,FileReadPost,BufNewFile,BufEnter * call system("tmux rename-window 'vim | " . expand("%:t") . "'")

autocmd VimLeave * call system("tmux setw automatic-rename")

" Always show statusline

set guifont=Inconsolata\ for\ Powerline:h15
let g:Powerline_symbols = 'fancy'
set encoding=utf-8
set t_Co=256
set fillchars+=stl:\ ,stlnc:\
set term=xterm-256color
set termencoding=utf-8

set laststatus=2

set clipboard+=unnamed

set backspace=2

set softtabstop=2
set shiftwidth=2
set expandtab

set backupdir=~/.vim/backup_files//
set directory=~/.vim/swap_files//
set undodir=~/.vim/undo_files//

highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/

set nu
set relativenumber
set nowrap

set viminfo+=n~/.vim/viminfo

autocmd FileType c setf cpp.root
autocmd BufNewFile,BufRead *.C set filetype=cpp.root
