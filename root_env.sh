#!/bin/zsh

export ROOTSYS="$HOME/Softwares/root"
export PATH="$ROOTSYS/bin:$PATH"
export PYTHONDIR="$ROOTSYS"
export LD_LIBRARY_PATH="$ROOTSYS/lib:$PYTHONDIR/lib:$ROOTSYS/bindings/pyroot:$LD_LIBRARY_PATH"
export PYTHONPATH="$ROOTSYS/lib:$PYTHONPATH:$ROOTSYS/bindings/pyroot"
source $ROOTSYS/bin/thisroot.sh
