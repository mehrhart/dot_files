######################
# oh-my-zsh settings #
######################

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
ZSH_THEME="ys"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="dd/mm/yyyy"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
if [ `uname` = "Darwin" ]; then
  plugins=(git osx iterm2)
else
  plugins=(git zsh-completions)
fi

source $ZSH/oh-my-zsh.sh

#------------------------------------------------------------------------------------

######################
# User configuration #
######################


export TERM=xterm-256color
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export EDITOR='vim'

# Common definitions

alias root="root -l"
alias cd="cd -P"
alias emacs="emacs -nw"
alias ltmux="tmux list-session"
alias atmux="tmux attach-session -t"

export SSH_KEY_PATH="~/.ssh/rsa_id"

autoload -U compinit && compinit

if [ `uname` = "Darwin" ]; then

  echo "ZSH Mac"

  # Fix ssh -Y issue on Mac
  alias ssh='ssh -o "XAuthLocation=/opt/X11/bin/xauth"'

  HOMEBREW_GITHUB_API_TOKEN=ee57717a91ce6ce6562f5243fac83573b9f339a9

  source /usr/local/bin/thisroot.sh

  alias jlab="ssh ehrhart@login.jlab.org"

elif [ `hostname` = "ipnphenp15" ]; then

  echo "ZSH IPN Laptop"

  #linuxbrew config
  #export PATH="$HOME/Softwares/linuxbrew/bin:$PATH"
  #export MANPATH="$(brew --prefix)/share/man:$MANPATH"
  #export INFOPATH="$(brew --prefix)/share/info:$INFOPATH"
  #export PATH="/vol0/mathieu/Softwares/linuxbrew/sbin:$PATH"

  #root setup
  alias root_env='. $HOME/.dot_files/root_env.sh'

  alias cache_clear='sudo zsh -c "sync; /bin/echo 3 > /proc/sys/vm/drop_caches"'
  
  #cowsay fun
  PATH="$PATH:/usr/games"
  alias cow='fortune -a -s | cowsay'
  fortune -a -s | cowsay
 

elif [[ `hostname` = *jlab.org* ]]; then

  echo "ZSH JLab"

  #JLab http proxy
  export https_proxy=https://jprox.jlab.org:8081
  export http_proxy=http://jprox.jlab.org:8081

  #ROOT setting
  export ROOTSYS=/u/apps/root/5.34.36/root
  export PATH=$ROOTSYS/bin:$PATH
  export LD_LIBRARY_PATH=$ROOTSYS/lib:$LD_LIBRARY_PATH

  alias tmux="/u/apps/tmux/PRO/bin/tmux"
  alias zsh="/bin/zsh"
  alias bjobs="jobstat -u ehrhart"

elif [[ `hostname` = cca* ]]; then

  #root setup
  pwd_tmp=`pwd`
  cd /pbs/software/centos-7-x86_64/root/6.10.02/bin
  source thisroot.cc.sh
  cd $pwd_tmp

elif [[ `hostname` = eic0 ]]; then

  #update-alernative
  export PATH="/home/ANL/ehrhart/software/bin:$PATH"
  alias update-my-alternatives='update-alternatives --altdir /home/ANL/ehrhart/software/etc/alternatives --admindir /home/ANL/ehrhart/software/var/lib/alternatives'


fi

